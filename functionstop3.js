const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

let l = 9;
let c = 0;
// criar 
// percorre as linhas
for (let i = 0; i < map.length; i++) {
    // encontra s
    if (map[i].indexOf('S') == 0) l = i;
    // percorre coluna map[i]
    for (let j = 0; j < map[l].length; j++) {
        // encontra s
        console.log(map[i][j].indexOf('S'))
        // s é = 0 colunas é igual a j
        if (map[l][j].indexOf('S') == 0) c = j;
    }
}

//console.log(l, c)
// printar o array no console
console.log(map)
// função printar blocos
function printBlocks() {
    // linha
    for (let i = 0; i < map.length; i++) {
        // pegar colunar
        for (let j = 0; j < map[i].length; j++) {
            // criar div
            let block = document.createElement('div')
            // criar classe do elemento
            block.className = 'blocks';
            // pegar pela div e anexar com appendChild
            document.getElementById('labirinto').appendChild(block)
            // se mapa linh/coluna for  ow, iguala o w = 9 eu capturo o w, estilizo com block.style
            if (map[i][j].indexOf('W') == 0) block.style.backgroundColor = 'red';
            // enxontrar vazio e estilizar
            else if (map[i][j].indexOf(' ') == 0) block.style.backgroundColor = 'blue';
            // encontrar s e estiilizar
            else if (map[i][j].indexOf('S') == 0)  block.style.backgroundColor = 'green'
            
            // encontrar f e estilizar
            else if (map[i][j].indexOf('F') == 0) block.style.backgroundColor = 'yellow';
        }
    }
}
// chamando a função, printa na tela
printBlocks();
let player = document.createElement('div')
player.id = 'divPlayer'
player.style.backgroundColor = 'green'
document.getElementById('Start').appendChild(player)

const blocks = document.querySelectorAll('.blocks')

document.addEventListener('keydown', (event) => {
    //recebe a tecla apertada
    const keyName = event.key;
    // arrowdown -> bottom
     // arrowdown -> bottom
     if (keyName == 'ArrowDown') {
         if (stop(l + 1,c)) {
          l += 1;
          printBlocks();
        }
    }  if(keyName == 'ArrowUp') {
        if (stop(l + 1,c)) {    
          l -= 1;
          printBlocks();
        }
    } if(keyName == 'ArrowRight') {
        if (stop(l, c + 1)) {    
          c += 1;
          printBlocks();
        }
    } if(keyName == 'ArrowLeft') {
        if (stop(l, c + 1)) {
          c -= 1;
          printBlocks();
        }
    }
});


function stop (l,c) {
    // se for diferente de w pode mover, se igual we nao move
    if ( c >= 0 && c < map.length && l >= 0 && l < map[0].length && map[c][l] != 'W') {
        return true
    } else {
        return false
    }
}