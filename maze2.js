const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

// If we search past the edge we'll get a null pointer error
const edgeX = map[0].length - 2;
const edgeY = map.length - 2;

// HORIZONTAL
// iterate each row
for(let y = 0; y < map.length; y++){

  // iterate each cell in the row
  for(let x = 0; x < edgeX; x++) {
    let cell = map[y][x];
    
    // Only check if cell is filled
    if(cell !== 0) {
      
      // Check the next two cells for the same value
      if(cell === map[y][x+1] && cell === map[y][x+2] ) {
        appendChild("3 in a row vertical found at " + (x+1) + ":" + (y+1))
      }
    }
  }
}

// VERTICAL
// iterate each row   
for(let y = 0; y < edgeY; y++){

  // iterate each cell in the row
  for(let x = 0; x < map[0].length; x++) {
    cell = map[y][x];
    
    // Only check if cell is filled
    if(cell !== 0) {
      
      // Check the next two cells for the same value
      if(cell === map[y+1][x] && cell === map[y+2][x] ) {
         let item = document.createElement('div');
         item.appendChild(document.createTextNode(map[i]));
         lista.appendChild(item);
        appendChild("3 in a row horizontal found at " + (x+1) + ":" + (y+1))
      }
    }
  }
}

// DIAGONAL (DOWN RIGHT)
// iterate each row   
for(let y = 0; y < edgeY; y++){

  // iterate each cell in the row
  for(let x = 0; x < edgeX; x++) {
    cell = map[y][x];
    
    // Only check if cell is filled
    if(cell !== 0) {
      
      // Check the next two cells for the same value
      if(cell === map[y+1][x+1] && cell === map[y+2][x+2] ) {
        appendChild("3 in a row down-right found at " + (x+1) + ":" + (y+1))
      }
    }
  }
}


// DIAGONAL (DOWN LEFT)
// iterate each row   
for(let y = 2; y < map.length; y++){

  // iterate each cell in the row
  for(let x = 0; x < edgeX; x++) {
    cell = map[y][x];
    
    // Only check if cell is filled
    if(cell !== 0) {
      
      // Check the next two cells for the same value
      if(cell === map[y-1][x+1] && cell === map[y-2][x+2] ) {
        appendChild("3 in a row down-left found at " + (x+1) + ":" + (y+1))
      }
    }
  }
}