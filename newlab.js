const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

let l = 0;
let c = 0;
// percorre as linhas
for (let i = 0; i < map.length; i++) {
    // encontra s
    if (map[i].indexOf('S') == 0) l = i;
    // percorre coluna map[i]
    for (let j = 0; j < map[l].length; j++) {
        // encontra s
        console.log(map[i][j].indexOf('S'))
        // s é = 0 colunas é igual a j
        if (map[l][j].indexOf('S') == 0) c = j;
    }
}

//console.log(l, c)
// printar o array no console
console.log(map)
// função printar blocos
function printBlocks() {
    for (let i = 0; i < map.length; i++) {
        for (let j = 0; j < map[i].length; j++) { 
            if (map[i][j].indexOf('W') == 0) {
                let parede = document.createElement('div')
                parede.className = 'paredes';
                document.getElementById('labirinto').appendChild(parede)
            }
            else if (map[i][j].indexOf(' ') == 0) {
                let vazio = document.createElement('div')
                vazio.className = 'vazios';
                document.getElementById('labirinto').appendChild(vazio)
            }
            else if (map[i][j].indexOf('S') == 0) {
                let S = document.createElement('div')
                S.id = 's';
                document.getElementById('labirinto').appendChild(S)
            }
            else if (map[i][j].indexOf('F') == 0) {
                let F = document.createElement('div')
                F.id = 'f';
                document.getElementById('labirinto').appendChild(F)
            }
        }
    }
}
// chamando a função, printa na tela
printBlocks();


const parede = document.querySelectorAll('.paredes')
const vazio = document.querySelectorAll('.vazios')
const S = document.querySelectorAll('#s')
const F = document.querySelectorAll('#f')
//console.log(blocks)
// utiliza o keyboardevents e move o bloco que eu capturei
document.addEventListener('keydown', (event) => {
    //recebe a tecla apertada
    const keyName = event.key;
    // arrowdown -> bottom
    if (keyName == "ArrowDown") {
        // movimenta no html
        let ind = 0;
        for (let i = 0; i < blocks.length; i++) {
            // se eu pegar os blocos verdes ele vai movimentar 
            if (blocks[i].style.backgroundColor == 'green') {
                ind = i;
                blocks[i].style.backgroundColor = 'blue';
            }
        }
        if(blocks[ind + 21].style.backgroundColor == 'red') {
            return; 
        }
        // mas apra movimentar, ele tem que partir do primeiro bloco azul
        blocks[ind + 21].style.backgroundColor = 'green';
        // arrow up -> top
    } else if (keyName == "ArrowUp") {
        let ind = 0;
        for (let i = 0; i < blocks.length; i++) {
            if (blocks[i].style.backgroundColor == 'green') {
                ind = i;
                blocks[i].style.backgroundColor = 'blue';
            }
        }
        blocks[ind - 21].style.backgroundColor = 'green';
        if(blocks[ind - 21].style.backgroundColor === 'red') {
            return;
        }
        // arrowright -> right
    } else if (keyName == "ArrowRight") {
        c++;
        map[l][c - 1] = ' ';
        map[l][c] = 'S';
        console.log(map)
        let ind = 0;
        for (let i = 0; i < blocks.length; i++) {
            if (blocks[i].style.backgroundColor == 'green') {
                ind = i;
                blocks[i].style.backgroundColor = 'blue';

            }
        }
        if(blocks[ind + 1].style.backgroundColor === 'red') {
            return;
        }
            console.log(blocks[ind + 1].style.backgroundColor = 'green');
            printBlocks();
            // arrowleft -> left
        } else if (keyName == "ArrowLeft") {
            let ind = 0;
            for (let i = 0; i < blocks.length; i++) {
                if (blocks[i].style.backgroundColor == 'green') {
                    ind = i;
                    blocks[i].style.backgroundColor = 'blue';
                }
            }
            if(blocks[ind - 1].style.backgroundColor === 'red') {
                return;
            }
                blocks[ind - 1].style.backgroundColor = 'green';
            }
        });