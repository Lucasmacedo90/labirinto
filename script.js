const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];
// map2 recebe map 1 para poder alterar
// mp2 para movimentar o bloco
// trocar letrar ' ' e 's'
let map2 = [...map]

let l = 9
let c = 0;
// percorre as linhas
// map[i][j]
for (let i = 0; i < map.length; i++) {
    // encontra s
    if (map[i].indexOf('S') == 0) l = i;
    // percorre coluna map[i]
    for (let j = 0; j < map[l].length; j++) {
        // encontra s
        console.log(map[i][j].indexOf('S'))
            // s é = 0 colunas é igual a j
        if (map[l][j].indexOf('S') == 0) c = j;
    }
}

//console.log(l, c)
// printar o array no console
console.log(map)
    // função printar blocos
function printBlocks(mapaAtual) {
    // linha
    for (let i = 0; i < mapaAtual.length; i++) {
        // pegar colunar
        for (let j = 0; j < mapaAtual[0].length; j++) {
            // criar div
            let block = document.createElement('div')
                // criar classe do elemento
            block.className = 'blocks';
            // pegar pela div e anexar com appendChild

            // se mapa linh/coluna for  ow, iguala o w = 9 eu capturo o w, estilizo com block.style
            if (mapaAtual[i][j] === "W") {
                console.log("IF = W /i = ", i + " j = ", j);
                block.style.backgroundColor = 'red';
                document.getElementById('labirinto').appendChild(block)
            }
            // enxontrar vazio e estilizar
            else if (mapaAtual[i][j] === " ") {
                console.log("IF = VAZIO /i = " + i + " j = ", j);
                block.style.backgroundColor = 'blue';
                document.getElementById('labirinto').appendChild(block)
            }
            // encontrar s e estiilizar
            else if (mapaAtual[i][j] === "S") {
                console.log("IF = S /i = ", i + " j = ", j);
                block.style.backgroundColor = 'green';
                document.getElementById('labirinto').appendChild(block)
            }
            // encontrar f e estilizar
            else if (mapaAtual[i][j] === 'F') {
                console.log("IF = F /i = ", i + " j = ", j);
                block.style.backgroundColor = 'yellow';
                document.getElementById('labirinto').appendChild(block)
            }
            // anexar por ultimo

            console.log(block.style.backgroundColor)
                // document.getElementById('labirinto').appendChild(block)
        }
    }
}
// chamando a função, printa na tela
printBlocks(map2);
// condição para se mover
// verificar se pode se mover para casa seguinte se for azul
//

const blocks = document.querySelectorAll('.blocks')
    //console.log(blocks)
    // utiliza o keyboardevents e move o bloco que eu capturei
document.addEventListener('keydown', (event) => {
    //recebe a tecla apertada
    const keyName = event.key;

    // arrowdown -> bottom
    if (keyName == "ArrowDown") {
        // movimenta no html
        let ind = 0;
        for (let i = 0; i < blocks.length; i++) {
            // se eu pegar os blocos verdes ele vai movimentar 
            if (blocks[i].style.backgroundColor == 'green') {
                ind = i;
                blocks[i].style.backgroundColor = 'blue';

            }
        }

        // mas apra movimentar, ele tem que partir do primeiro bloco azul
        blocks[ind + 21].style.backgroundColor = 'green';

        // arrow up -> top
    } else if (keyName == "ArrowUp") {
        let ind = 0;
        for (let i = 0; i < blocks.length; i++) {
            if (blocks[i].style.backgroundColor == 'green') {
                ind = i;
                blocks[i].style.backgroundColor = 'blue';

            }
        }

        blocks[ind - 21].style.backgroundColor = 'green';


        // arrowright -> right
    } else if (keyName == "ArrowRight") {
        console.log("ENTROU ARROW RIGHT");
        // c = coluna atual = "S"
        // l = linha atual = "S"
        // 1º Verificar se a posição seguinte é valida
        if (c + 1 != undefined) {
            console.log("ENTROU IF UNDEFINED, c = " + c + "l = ", l);
            if (map2[c + 1][l] === " " || map2[c + 1][l] === "F") {
                if (map2[c + 1][l] === "F") {
                    //Funçao de vitoria
                    console.log("ENTROU IF = F");
                    printBlocks(map2);
                } else {
                    console.log("ELSE != F");
                    let arrayMap2 = map2[l].split("");
                    console.log(arrayMap2);
                    arrayMap2[c] = " "
                    arrayMap2[c + 1] = "S";

                    map2[l] = arrayMap2.join("");

                    printBlocks(map2);
                    c++;
                }

            }
        }

        // arrowleft -> left
    } else if (keyName == "ArrowLeft") {
        let ind = 0;
        for (let i = 0; i < blocks.length; i++) {
            if (blocks[i].style.backgroundColor == 'red') {
                return
            }
            if (blocks[i].style.backgroundColor == 'green') {
                ind = i;
                blocks[i].style.backgroundColor = 'blue';

            }
        }
        blocks[ind - 1].style.backgroundColor = 'green';

    }
});