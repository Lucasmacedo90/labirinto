function printBlocks() {
    // linha
    for (let i = 0; i < map.length; i++) {
        // pegar colunar
        for (let j = 0; j < map[i].length; j++) {
            // criar div
            let block = document.createElement('div')
            // criar classe do elemento
            block.className = 'blocks';
            
            document.getElementById('labirinto').appendChild(block)
            // se mapa linh/coluna for  ow, iguala o w = 9 eu capturo o w, estilizo com block.style
            if (map[i][j].indexOf('W') == 0) block.style.backgroundColor = 'red';
            // enxontrar vazio e estilizar
            else if (map[i][j].indexOf(' ') == 0) block.style.backgroundColor = 'blue';
            // encontrar s e estiilizar
            else if (map[i][j].indexOf('S') == 0) block.style.backgroundColor = 'green';
            // encontrar f e estilizar
            else if (map[i][j].indexOf('F') == 0) block.style.backgroundColor = 'yellow';
        }
    }
}
// chamando a função, printa na tela
printBlocks();


const blocks = document.querySelectorAll('.blocks')